app_name = "client_directory"
app_title = "Client Directory"
app_publisher = "Enginade"
app_description = "Corporate Client Directory"
app_icon = "octicon octicon-organization"
app_color = "#E74C3C"
app_email = "enginade@gmail.com"
app_version = "0.0.1"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/client_directory/css/client_directory.css"
# app_include_js = "/assets/client_directory/js/client_directory.js"

# include js, css files in header of web template
# web_include_css = "/assets/client_directory/css/client_directory.css"
# web_include_js = "/assets/client_directory/js/client_directory.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "client_directory.install.before_install"
# after_install = "client_directory.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "client_directory.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.core.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.core.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"client_directory.tasks.all"
# 	],
# 	"daily": [
# 		"client_directory.tasks.daily"
# 	],
# 	"hourly": [
# 		"client_directory.tasks.hourly"
# 	],
# 	"weekly": [
# 		"client_directory.tasks.weekly"
# 	]
# 	"monthly": [
# 		"client_directory.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "client_directory.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.core.doctype.event.event.get_events": "client_directory.event.get_events"
# }


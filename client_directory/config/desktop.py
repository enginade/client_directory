from frappe import _

def get_data():
	return {
		"Client Directory": {
			"color": "#E74C3C",
			"icon": "octicon octicon-organization",
			"type": "module",
			"label": _("Client Directory")
		}
	}
